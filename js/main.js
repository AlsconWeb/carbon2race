jQuery(document).ready(function($) {
	/**
	 * Open on mobile add to cart.
	 */
	$('.sticky-add-to-cart-wrapper .label-button').click(function(){
		$(this).parents('.sticky-add-to-cart--active').toggleClass('open');
	})

	let select = $('.variation-select select');
	select.each( function(i, el){
		let attribute = [];
		$(el.options).each(function( index, element ){
			attribute.push($(element).attr('value'));
		});

		attribute = attribute.filter(String);
		$(el).parent().find('.variable-items-wrapper').attr('data-attribute_values', '["'+attribute.join('","')+'"]');
	});

	/**
	 * Remove and add Active class and value.
	 */
	$('.variation-select .variable-item').click(function(e){
		let selectID = $(this).parent().parent().find('select').attr('id');

		let value = $(this).data('value');
		$('#'+selectID).val(value).trigger('change');
		$('#'+selectID).trigger('click');

		if($(this).hasClass('selected')){
			console.log('ddd');
		   $(this).attr('class', 'variable-item button-variable-item');
		}else{
			$(this).parent().find('li').each(function( i, el ){
				$(el).attr('class', 'variable-item button-variable-item');
				console.log('sss');
			});
			$(this).attr('class','variable-item button-variable-item selected');
		}
	});
	
	setTimeout(
		function(  ){
			$('.woo-variation-items-wrapper .variable-item').click(function( e ){
				let idEL = $(this).parent().prev().data('attribute_name');
				let value = $(this).data('value')
				$('[data-attribute_name="'+idEL+'"').val(value)
			})
		},
		1000
	)

	/**
	 * Add test to label.
	 */
	$('.tm-extra-product-options-radio .tc-label').click(function( e ){
		let text = $(this).text();
		if($(this).parents('.Logosticker-div').find('.tm-epo-element-label span').length){
			$(this).parents('.Logosticker-div').find('.tm-epo-element-label span').text(' : ' + text)
		}else{
			$(this).parents('.Logosticker-div').find('.tm-epo-element-label').append('<span' +
			' class="woo-selected-variation-item-name">' +
			' : '+text+'</span>')
		}
	})
});