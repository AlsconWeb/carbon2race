<?php

// add_filter( 'woocommerce_variable_price_html', 'bbloomer_variation_price_format_310', 10, 2 );

function bbloomer_variation_price_format_310( $price, $product ) {

// 1. Find the minimum regular and sale prices

	$min_var_reg_price  = $product->get_variation_regular_price( 'max', true );
	$min_var_sale_price = $product->get_variation_sale_price( 'min', true );

// 2. New $price

	if ( $min_var_sale_price ) {
		$price = sprintf( __( 'From: <del>%1$s</del><ins>%2$s</ins>', 'woocommerce' ), wc_price( $min_var_reg_price ), wc_price( $min_var_sale_price ) );
	} else {
		$price = sprintf( __( 'From: %1$s', 'woocommerce' ), wc_price( $min_var_reg_price ) );
	}

// 3. Return edited $price

	return $price;
}

add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );

/** Disable Ajax Call from WooCommerce on front page and posts*/
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11 );
function dequeue_woocommerce_cart_fragments() {
	if ( is_front_page() || is_single() ) {
		wp_dequeue_script( 'wc-cart-fragments' );
	}
}

/* Free Shipping Code start here */
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = [];
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}

	return ! empty( $free ) ? $free : $rates;
}

add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 ); // ---------------------------------------------
add_action( 'woocommerce_before_cart', 'move_proceed_button' );
function move_proceed_button( $checkout ) {
	echo '<div class="mobile-checkout-btn text-right"><a href="' . esc_url( WC()->cart->get_checkout_url() ) . '" class="checkout-button button alt wc-forward" >' . __( 'Proceed to Checkout', 'woocommerce' ) . '</a></div>';
}

/** Remove QTY from product page*/
function quantity_wp_head() {

	if ( is_product() ) {
		?>
		<style type="text/css">.quantity, .buttons_added {
				width: 0;
				height: 0;
				display: none;
				visibility: hidden;
			}</style>
		<?php
	}
}

add_action( 'wp_head', 'quantity_wp_head' );

/**Change place order text */

add_filter( 'woocommerce_order_button_text', 'njengah_change_checkout_button_text' );

function njengah_change_checkout_button_text( $button_text ) {

	return 'Buy Now'; // Replace this text in quotes with your respective custom button text

}

/**Popravek prikaza "variacij" produkta */
add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );


/*custom js*/

function wpdocs_selectively_enqueue_admin_script( $hook ) {
	if ( 'edit.php' != $hook ) {
		return;
	}
	//wp_enqueue_script( 'my_custom_script', plugin_dir_url( __FILE__ ) . 'myscript.js', array(), '1.0' );
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/myscript.js', [ 'jquery' ], 1.1, true );
}

add_action( 'admin_enqueue_scripts', 'wpdocs_selectively_enqueue_admin_script' );

/**Popravek prikaza "on backorder text-a */
function alt_message() {
	return "This item will ship in 5-7 business days";
}

function backorder_text( $availability ) {
	$altmessage = alt_message();
	foreach ( $availability as $i ) {
		$availability = str_replace( 'Available on backorder', $altmessage, $availability );
	}

	return $availability;
}

add_filter( 'woocommerce_get_availability', 'backorder_text' );

function woocommerce_custom_cart_item_name( $_product_title, $cart_item, $cart_item_key ) {
	$altmessage = alt_message();
	if ( $cart_item['data']->backorders_require_notification() && $cart_item['data']->is_on_backorder( $cart_item['quantity'] ) ) {
		$_product_title .= __( ' - ' . $altmessage, 'woocommerce' );
	}

	return $_product_title;
}

add_filter( 'woocommerce_cart_item_name', 'woocommerce_custom_cart_item_name', 10, 3 );

/**Popravek prikaza "only one left in stock - remove can be backordered */
add_filter( 'woocommerce_get_availability_text', 'filter_product_availability_text', 10, 2 );
function filter_product_availability_text( $availability, $product ) {

	if ( $product->backorders_require_notification() ) {
		$availability = str_replace( '(can be backordered)', '', $availability );
	}

	return $availability;
}

/**
 * Start Alex L.
 */


/**
 * Remove Sticky add to Cart.
 */
function init_child_themes() {
	remove_action( 'woocommerce_before_add_to_cart_button', 'flatsome_sticky_add_to_cart_before', - 100 );

	add_action( 'woocommerce_before_add_to_cart_button', 'iwp_add_sticky_add_to_cart_before', - 100 );
}

add_action( 'init', 'init_child_themes' );


function iwp_add_sticky_add_to_cart_before() {
	global $product;

	if ( ! is_product() || ! get_theme_mod( 'product_sticky_cart', 0 ) || ! apply_filters( 'flatsome_sticky_add_to_cart_enabled', true, $product ) ) {
		return;
	}
	?>

	<div class="sticky-add-to-cart-wrapper">
	<div class="sticky-add-to-cart">
	<div class="sticky-add-to-cart__product">
		<?php
		$image_id = $product->get_image_id();
		$image    = wp_get_attachment_image_src( $image_id, 'woocommerce_gallery_thumbnail' );
		if ( $image ) {
			$image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
			$image     = '<img src="' . $image[0] . '" alt="' . $image_alt . '" class="sticky-add-to-cart-img" />';
			echo $image;
		}
		?>
		<div class="product-title-small hide-for-small">
			<strong><?php echo esc_html( get_the_title() ); ?></strong>
		</div>
		<?php foreach ( $product->get_attributes() as $attribute_name => $options ) : ?>
			<div class="variation-select">

				<label for="<?php echo sanitize_title( $attribute_name ); ?>"><?php echo wc_attribute_label( $attribute_name ); ?></label></td>

				<?php
				$selected = isset( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ? wc_clean( urldecode( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ) : $product->get_variation_default_attribute( $attribute_name );
				wc_dropdown_variation_attribute_options( [
					'attribute' => $attribute_name,
					'product'   => $product,
					'selected'  => $selected,
				] );
				?>


			</div>
		<?php endforeach; ?>
		<?php
		if ( ! $product->is_type( 'variable' ) ) {
			woocommerce_template_single_price();
		}
		?>
	</div>
	<div class="label-button">
		<button type="button" class="button"><?php esc_html_e( 'Add to Cart' ); ?></button>
	</div>
	<?php
}

/**
 * Add Scripts.
 */
function iwp_add_scripts() {
	wp_enqueue_script( 'iwp-main', get_stylesheet_directory_uri() . '/js/main.js', [ 'jquery' ], '', true );
}

add_action( 'wp_enqueue_scripts', 'iwp_add_scripts' );


/**
 * Always display input In Stock.
 *
 * @param $availability
 * @param $_product
 *
 * @return mixed
 */
function custom_override_get_availability( $availability, $_product ) {
	$prod = $_product->get_data();

	if ( $prod['manage_stock'] &&  $prod['stock_quantity'] ) {
		return $availability;
	} else {
		$availability['availability'] = __('This item will ship in 5-7 business days', 'woocommerce');
		$availability['class'] ='available-on-backorder';
	}


	return $availability;
}

add_filter( 'woocommerce_get_availability', 'custom_override_get_availability', 10, 2 );
